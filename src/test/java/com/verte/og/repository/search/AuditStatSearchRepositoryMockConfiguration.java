package com.verte.og.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of AuditStatSearchRepository to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class AuditStatSearchRepositoryMockConfiguration {

    @MockBean
    private AuditStatSearchRepository mockAuditStatSearchRepository;

}
