package com.verte.og.web.rest;

import com.verte.og.OrderGenApp;

import com.verte.og.domain.AuditStat;
import com.verte.og.repository.AuditStatRepository;
import com.verte.og.repository.search.AuditStatSearchRepository;
import com.verte.og.service.AuditStatService;
import com.verte.og.service.dto.AuditStatDTO;
import com.verte.og.service.mapper.AuditStatMapper;
import com.verte.og.web.rest.errors.ExceptionTranslator;
import com.verte.og.service.dto.AuditStatCriteria;
import com.verte.og.service.AuditStatQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;


import static com.verte.og.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AuditStatResource REST controller.
 *
 * @see AuditStatResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OrderGenApp.class)
public class AuditStatResourceIntTest {

    private static final String DEFAULT_RUN_ID = "AAAAAAAAAA";
    private static final String UPDATED_RUN_ID = "BBBBBBBBBB";

    private static final Long DEFAULT_EXECUTIONTIME = 1L;
    private static final Long UPDATED_EXECUTIONTIME = 2L;

    private static final String DEFAULT_ERROR = "AAAAAAAAAA";
    private static final String UPDATED_ERROR = "BBBBBBBBBB";

    private static final String DEFAULT_INPUT = "AAAAAAAAAA";
    private static final String UPDATED_INPUT = "BBBBBBBBBB";

    private static final String DEFAULT_OUTPUT = "AAAAAAAAAA";
    private static final String UPDATED_OUTPUT = "BBBBBBBBBB";

    private static final Instant DEFAULT_START_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_START_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_END_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_END_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_RUNCONFIG = "AAAAAAAAAA";
    private static final String UPDATED_RUNCONFIG = "BBBBBBBBBB";

    @Autowired
    private AuditStatRepository auditStatRepository;

    @Autowired
    private AuditStatMapper auditStatMapper;

    @Autowired
    private AuditStatService auditStatService;

    /**
     * This repository is mocked in the com.verte.og.repository.search test package.
     *
     * @see com.verte.og.repository.search.AuditStatSearchRepositoryMockConfiguration
     */
    @Autowired
    private AuditStatSearchRepository mockAuditStatSearchRepository;

    @Autowired
    private AuditStatQueryService auditStatQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAuditStatMockMvc;

    private AuditStat auditStat;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AuditStatResource auditStatResource = new AuditStatResource(auditStatService, auditStatQueryService);
        this.restAuditStatMockMvc = MockMvcBuilders.standaloneSetup(auditStatResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AuditStat createEntity(EntityManager em) {
        AuditStat auditStat = new AuditStat()
            .runId(DEFAULT_RUN_ID)
            .executiontime(DEFAULT_EXECUTIONTIME)
            .error(DEFAULT_ERROR)
            .input(DEFAULT_INPUT)
            .output(DEFAULT_OUTPUT)
            .startTime(DEFAULT_START_TIME)
            .endTime(DEFAULT_END_TIME)
            .runconfig(DEFAULT_RUNCONFIG);
        return auditStat;
    }

    @Before
    public void initTest() {
        auditStat = createEntity(em);
    }

    @Test
    @Transactional
    public void createAuditStat() throws Exception {
        int databaseSizeBeforeCreate = auditStatRepository.findAll().size();

        // Create the AuditStat
        AuditStatDTO auditStatDTO = auditStatMapper.toDto(auditStat);
        restAuditStatMockMvc.perform(post("/api/audit-stats")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(auditStatDTO)))
            .andExpect(status().isCreated());

        // Validate the AuditStat in the database
        List<AuditStat> auditStatList = auditStatRepository.findAll();
        assertThat(auditStatList).hasSize(databaseSizeBeforeCreate + 1);
        AuditStat testAuditStat = auditStatList.get(auditStatList.size() - 1);
        assertThat(testAuditStat.getRunId()).isEqualTo(DEFAULT_RUN_ID);
        assertThat(testAuditStat.getExecutiontime()).isEqualTo(DEFAULT_EXECUTIONTIME);
        assertThat(testAuditStat.getError()).isEqualTo(DEFAULT_ERROR);
        assertThat(testAuditStat.getInput()).isEqualTo(DEFAULT_INPUT);
        assertThat(testAuditStat.getOutput()).isEqualTo(DEFAULT_OUTPUT);
        assertThat(testAuditStat.getStartTime()).isEqualTo(DEFAULT_START_TIME);
        assertThat(testAuditStat.getEndTime()).isEqualTo(DEFAULT_END_TIME);
        assertThat(testAuditStat.getRunconfig()).isEqualTo(DEFAULT_RUNCONFIG);

        // Validate the AuditStat in Elasticsearch
        verify(mockAuditStatSearchRepository, times(1)).save(testAuditStat);
    }

    @Test
    @Transactional
    public void createAuditStatWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = auditStatRepository.findAll().size();

        // Create the AuditStat with an existing ID
        auditStat.setId(1L);
        AuditStatDTO auditStatDTO = auditStatMapper.toDto(auditStat);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAuditStatMockMvc.perform(post("/api/audit-stats")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(auditStatDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AuditStat in the database
        List<AuditStat> auditStatList = auditStatRepository.findAll();
        assertThat(auditStatList).hasSize(databaseSizeBeforeCreate);

        // Validate the AuditStat in Elasticsearch
        verify(mockAuditStatSearchRepository, times(0)).save(auditStat);
    }

    @Test
    @Transactional
    public void getAllAuditStats() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get all the auditStatList
        restAuditStatMockMvc.perform(get("/api/audit-stats?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(auditStat.getId().intValue())))
            .andExpect(jsonPath("$.[*].runId").value(hasItem(DEFAULT_RUN_ID.toString())))
            .andExpect(jsonPath("$.[*].executiontime").value(hasItem(DEFAULT_EXECUTIONTIME.intValue())))
            .andExpect(jsonPath("$.[*].error").value(hasItem(DEFAULT_ERROR.toString())))
            .andExpect(jsonPath("$.[*].input").value(hasItem(DEFAULT_INPUT.toString())))
            .andExpect(jsonPath("$.[*].output").value(hasItem(DEFAULT_OUTPUT.toString())))
            .andExpect(jsonPath("$.[*].startTime").value(hasItem(DEFAULT_START_TIME.toString())))
            .andExpect(jsonPath("$.[*].endTime").value(hasItem(DEFAULT_END_TIME.toString())))
            .andExpect(jsonPath("$.[*].runconfig").value(hasItem(DEFAULT_RUNCONFIG.toString())));
    }
    
    @Test
    @Transactional
    public void getAuditStat() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get the auditStat
        restAuditStatMockMvc.perform(get("/api/audit-stats/{id}", auditStat.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(auditStat.getId().intValue()))
            .andExpect(jsonPath("$.runId").value(DEFAULT_RUN_ID.toString()))
            .andExpect(jsonPath("$.executiontime").value(DEFAULT_EXECUTIONTIME.intValue()))
            .andExpect(jsonPath("$.error").value(DEFAULT_ERROR.toString()))
            .andExpect(jsonPath("$.input").value(DEFAULT_INPUT.toString()))
            .andExpect(jsonPath("$.output").value(DEFAULT_OUTPUT.toString()))
            .andExpect(jsonPath("$.startTime").value(DEFAULT_START_TIME.toString()))
            .andExpect(jsonPath("$.endTime").value(DEFAULT_END_TIME.toString()))
            .andExpect(jsonPath("$.runconfig").value(DEFAULT_RUNCONFIG.toString()));
    }

    @Test
    @Transactional
    public void getAllAuditStatsByRunIdIsEqualToSomething() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get all the auditStatList where runId equals to DEFAULT_RUN_ID
        defaultAuditStatShouldBeFound("runId.equals=" + DEFAULT_RUN_ID);

        // Get all the auditStatList where runId equals to UPDATED_RUN_ID
        defaultAuditStatShouldNotBeFound("runId.equals=" + UPDATED_RUN_ID);
    }

    @Test
    @Transactional
    public void getAllAuditStatsByRunIdIsInShouldWork() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get all the auditStatList where runId in DEFAULT_RUN_ID or UPDATED_RUN_ID
        defaultAuditStatShouldBeFound("runId.in=" + DEFAULT_RUN_ID + "," + UPDATED_RUN_ID);

        // Get all the auditStatList where runId equals to UPDATED_RUN_ID
        defaultAuditStatShouldNotBeFound("runId.in=" + UPDATED_RUN_ID);
    }

    @Test
    @Transactional
    public void getAllAuditStatsByRunIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get all the auditStatList where runId is not null
        defaultAuditStatShouldBeFound("runId.specified=true");

        // Get all the auditStatList where runId is null
        defaultAuditStatShouldNotBeFound("runId.specified=false");
    }

    @Test
    @Transactional
    public void getAllAuditStatsByExecutiontimeIsEqualToSomething() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get all the auditStatList where executiontime equals to DEFAULT_EXECUTIONTIME
        defaultAuditStatShouldBeFound("executiontime.equals=" + DEFAULT_EXECUTIONTIME);

        // Get all the auditStatList where executiontime equals to UPDATED_EXECUTIONTIME
        defaultAuditStatShouldNotBeFound("executiontime.equals=" + UPDATED_EXECUTIONTIME);
    }

    @Test
    @Transactional
    public void getAllAuditStatsByExecutiontimeIsInShouldWork() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get all the auditStatList where executiontime in DEFAULT_EXECUTIONTIME or UPDATED_EXECUTIONTIME
        defaultAuditStatShouldBeFound("executiontime.in=" + DEFAULT_EXECUTIONTIME + "," + UPDATED_EXECUTIONTIME);

        // Get all the auditStatList where executiontime equals to UPDATED_EXECUTIONTIME
        defaultAuditStatShouldNotBeFound("executiontime.in=" + UPDATED_EXECUTIONTIME);
    }

    @Test
    @Transactional
    public void getAllAuditStatsByExecutiontimeIsNullOrNotNull() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get all the auditStatList where executiontime is not null
        defaultAuditStatShouldBeFound("executiontime.specified=true");

        // Get all the auditStatList where executiontime is null
        defaultAuditStatShouldNotBeFound("executiontime.specified=false");
    }

    @Test
    @Transactional
    public void getAllAuditStatsByExecutiontimeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get all the auditStatList where executiontime greater than or equals to DEFAULT_EXECUTIONTIME
        defaultAuditStatShouldBeFound("executiontime.greaterOrEqualThan=" + DEFAULT_EXECUTIONTIME);

        // Get all the auditStatList where executiontime greater than or equals to UPDATED_EXECUTIONTIME
        defaultAuditStatShouldNotBeFound("executiontime.greaterOrEqualThan=" + UPDATED_EXECUTIONTIME);
    }

    @Test
    @Transactional
    public void getAllAuditStatsByExecutiontimeIsLessThanSomething() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get all the auditStatList where executiontime less than or equals to DEFAULT_EXECUTIONTIME
        defaultAuditStatShouldNotBeFound("executiontime.lessThan=" + DEFAULT_EXECUTIONTIME);

        // Get all the auditStatList where executiontime less than or equals to UPDATED_EXECUTIONTIME
        defaultAuditStatShouldBeFound("executiontime.lessThan=" + UPDATED_EXECUTIONTIME);
    }


    @Test
    @Transactional
    public void getAllAuditStatsByErrorIsEqualToSomething() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get all the auditStatList where error equals to DEFAULT_ERROR
        defaultAuditStatShouldBeFound("error.equals=" + DEFAULT_ERROR);

        // Get all the auditStatList where error equals to UPDATED_ERROR
        defaultAuditStatShouldNotBeFound("error.equals=" + UPDATED_ERROR);
    }

    @Test
    @Transactional
    public void getAllAuditStatsByErrorIsInShouldWork() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get all the auditStatList where error in DEFAULT_ERROR or UPDATED_ERROR
        defaultAuditStatShouldBeFound("error.in=" + DEFAULT_ERROR + "," + UPDATED_ERROR);

        // Get all the auditStatList where error equals to UPDATED_ERROR
        defaultAuditStatShouldNotBeFound("error.in=" + UPDATED_ERROR);
    }

    @Test
    @Transactional
    public void getAllAuditStatsByErrorIsNullOrNotNull() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get all the auditStatList where error is not null
        defaultAuditStatShouldBeFound("error.specified=true");

        // Get all the auditStatList where error is null
        defaultAuditStatShouldNotBeFound("error.specified=false");
    }

    @Test
    @Transactional
    public void getAllAuditStatsByInputIsEqualToSomething() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get all the auditStatList where input equals to DEFAULT_INPUT
        defaultAuditStatShouldBeFound("input.equals=" + DEFAULT_INPUT);

        // Get all the auditStatList where input equals to UPDATED_INPUT
        defaultAuditStatShouldNotBeFound("input.equals=" + UPDATED_INPUT);
    }

    @Test
    @Transactional
    public void getAllAuditStatsByInputIsInShouldWork() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get all the auditStatList where input in DEFAULT_INPUT or UPDATED_INPUT
        defaultAuditStatShouldBeFound("input.in=" + DEFAULT_INPUT + "," + UPDATED_INPUT);

        // Get all the auditStatList where input equals to UPDATED_INPUT
        defaultAuditStatShouldNotBeFound("input.in=" + UPDATED_INPUT);
    }

    @Test
    @Transactional
    public void getAllAuditStatsByInputIsNullOrNotNull() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get all the auditStatList where input is not null
        defaultAuditStatShouldBeFound("input.specified=true");

        // Get all the auditStatList where input is null
        defaultAuditStatShouldNotBeFound("input.specified=false");
    }

    @Test
    @Transactional
    public void getAllAuditStatsByOutputIsEqualToSomething() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get all the auditStatList where output equals to DEFAULT_OUTPUT
        defaultAuditStatShouldBeFound("output.equals=" + DEFAULT_OUTPUT);

        // Get all the auditStatList where output equals to UPDATED_OUTPUT
        defaultAuditStatShouldNotBeFound("output.equals=" + UPDATED_OUTPUT);
    }

    @Test
    @Transactional
    public void getAllAuditStatsByOutputIsInShouldWork() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get all the auditStatList where output in DEFAULT_OUTPUT or UPDATED_OUTPUT
        defaultAuditStatShouldBeFound("output.in=" + DEFAULT_OUTPUT + "," + UPDATED_OUTPUT);

        // Get all the auditStatList where output equals to UPDATED_OUTPUT
        defaultAuditStatShouldNotBeFound("output.in=" + UPDATED_OUTPUT);
    }

    @Test
    @Transactional
    public void getAllAuditStatsByOutputIsNullOrNotNull() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get all the auditStatList where output is not null
        defaultAuditStatShouldBeFound("output.specified=true");

        // Get all the auditStatList where output is null
        defaultAuditStatShouldNotBeFound("output.specified=false");
    }

    @Test
    @Transactional
    public void getAllAuditStatsByStartTimeIsEqualToSomething() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get all the auditStatList where startTime equals to DEFAULT_START_TIME
        defaultAuditStatShouldBeFound("startTime.equals=" + DEFAULT_START_TIME);

        // Get all the auditStatList where startTime equals to UPDATED_START_TIME
        defaultAuditStatShouldNotBeFound("startTime.equals=" + UPDATED_START_TIME);
    }

    @Test
    @Transactional
    public void getAllAuditStatsByStartTimeIsInShouldWork() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get all the auditStatList where startTime in DEFAULT_START_TIME or UPDATED_START_TIME
        defaultAuditStatShouldBeFound("startTime.in=" + DEFAULT_START_TIME + "," + UPDATED_START_TIME);

        // Get all the auditStatList where startTime equals to UPDATED_START_TIME
        defaultAuditStatShouldNotBeFound("startTime.in=" + UPDATED_START_TIME);
    }

    @Test
    @Transactional
    public void getAllAuditStatsByStartTimeIsNullOrNotNull() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get all the auditStatList where startTime is not null
        defaultAuditStatShouldBeFound("startTime.specified=true");

        // Get all the auditStatList where startTime is null
        defaultAuditStatShouldNotBeFound("startTime.specified=false");
    }

    @Test
    @Transactional
    public void getAllAuditStatsByEndTimeIsEqualToSomething() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get all the auditStatList where endTime equals to DEFAULT_END_TIME
        defaultAuditStatShouldBeFound("endTime.equals=" + DEFAULT_END_TIME);

        // Get all the auditStatList where endTime equals to UPDATED_END_TIME
        defaultAuditStatShouldNotBeFound("endTime.equals=" + UPDATED_END_TIME);
    }

    @Test
    @Transactional
    public void getAllAuditStatsByEndTimeIsInShouldWork() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get all the auditStatList where endTime in DEFAULT_END_TIME or UPDATED_END_TIME
        defaultAuditStatShouldBeFound("endTime.in=" + DEFAULT_END_TIME + "," + UPDATED_END_TIME);

        // Get all the auditStatList where endTime equals to UPDATED_END_TIME
        defaultAuditStatShouldNotBeFound("endTime.in=" + UPDATED_END_TIME);
    }

    @Test
    @Transactional
    public void getAllAuditStatsByEndTimeIsNullOrNotNull() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get all the auditStatList where endTime is not null
        defaultAuditStatShouldBeFound("endTime.specified=true");

        // Get all the auditStatList where endTime is null
        defaultAuditStatShouldNotBeFound("endTime.specified=false");
    }

    @Test
    @Transactional
    public void getAllAuditStatsByRunconfigIsEqualToSomething() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get all the auditStatList where runconfig equals to DEFAULT_RUNCONFIG
        defaultAuditStatShouldBeFound("runconfig.equals=" + DEFAULT_RUNCONFIG);

        // Get all the auditStatList where runconfig equals to UPDATED_RUNCONFIG
        defaultAuditStatShouldNotBeFound("runconfig.equals=" + UPDATED_RUNCONFIG);
    }

    @Test
    @Transactional
    public void getAllAuditStatsByRunconfigIsInShouldWork() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get all the auditStatList where runconfig in DEFAULT_RUNCONFIG or UPDATED_RUNCONFIG
        defaultAuditStatShouldBeFound("runconfig.in=" + DEFAULT_RUNCONFIG + "," + UPDATED_RUNCONFIG);

        // Get all the auditStatList where runconfig equals to UPDATED_RUNCONFIG
        defaultAuditStatShouldNotBeFound("runconfig.in=" + UPDATED_RUNCONFIG);
    }

    @Test
    @Transactional
    public void getAllAuditStatsByRunconfigIsNullOrNotNull() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        // Get all the auditStatList where runconfig is not null
        defaultAuditStatShouldBeFound("runconfig.specified=true");

        // Get all the auditStatList where runconfig is null
        defaultAuditStatShouldNotBeFound("runconfig.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultAuditStatShouldBeFound(String filter) throws Exception {
        restAuditStatMockMvc.perform(get("/api/audit-stats?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(auditStat.getId().intValue())))
            .andExpect(jsonPath("$.[*].runId").value(hasItem(DEFAULT_RUN_ID)))
            .andExpect(jsonPath("$.[*].executiontime").value(hasItem(DEFAULT_EXECUTIONTIME.intValue())))
            .andExpect(jsonPath("$.[*].error").value(hasItem(DEFAULT_ERROR)))
            .andExpect(jsonPath("$.[*].input").value(hasItem(DEFAULT_INPUT)))
            .andExpect(jsonPath("$.[*].output").value(hasItem(DEFAULT_OUTPUT)))
            .andExpect(jsonPath("$.[*].startTime").value(hasItem(DEFAULT_START_TIME.toString())))
            .andExpect(jsonPath("$.[*].endTime").value(hasItem(DEFAULT_END_TIME.toString())))
            .andExpect(jsonPath("$.[*].runconfig").value(hasItem(DEFAULT_RUNCONFIG)));

        // Check, that the count call also returns 1
        restAuditStatMockMvc.perform(get("/api/audit-stats/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultAuditStatShouldNotBeFound(String filter) throws Exception {
        restAuditStatMockMvc.perform(get("/api/audit-stats?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAuditStatMockMvc.perform(get("/api/audit-stats/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingAuditStat() throws Exception {
        // Get the auditStat
        restAuditStatMockMvc.perform(get("/api/audit-stats/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAuditStat() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        int databaseSizeBeforeUpdate = auditStatRepository.findAll().size();

        // Update the auditStat
        AuditStat updatedAuditStat = auditStatRepository.findById(auditStat.getId()).get();
        // Disconnect from session so that the updates on updatedAuditStat are not directly saved in db
        em.detach(updatedAuditStat);
        updatedAuditStat
            .runId(UPDATED_RUN_ID)
            .executiontime(UPDATED_EXECUTIONTIME)
            .error(UPDATED_ERROR)
            .input(UPDATED_INPUT)
            .output(UPDATED_OUTPUT)
            .startTime(UPDATED_START_TIME)
            .endTime(UPDATED_END_TIME)
            .runconfig(UPDATED_RUNCONFIG);
        AuditStatDTO auditStatDTO = auditStatMapper.toDto(updatedAuditStat);

        restAuditStatMockMvc.perform(put("/api/audit-stats")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(auditStatDTO)))
            .andExpect(status().isOk());

        // Validate the AuditStat in the database
        List<AuditStat> auditStatList = auditStatRepository.findAll();
        assertThat(auditStatList).hasSize(databaseSizeBeforeUpdate);
        AuditStat testAuditStat = auditStatList.get(auditStatList.size() - 1);
        assertThat(testAuditStat.getRunId()).isEqualTo(UPDATED_RUN_ID);
        assertThat(testAuditStat.getExecutiontime()).isEqualTo(UPDATED_EXECUTIONTIME);
        assertThat(testAuditStat.getError()).isEqualTo(UPDATED_ERROR);
        assertThat(testAuditStat.getInput()).isEqualTo(UPDATED_INPUT);
        assertThat(testAuditStat.getOutput()).isEqualTo(UPDATED_OUTPUT);
        assertThat(testAuditStat.getStartTime()).isEqualTo(UPDATED_START_TIME);
        assertThat(testAuditStat.getEndTime()).isEqualTo(UPDATED_END_TIME);
        assertThat(testAuditStat.getRunconfig()).isEqualTo(UPDATED_RUNCONFIG);

        // Validate the AuditStat in Elasticsearch
        verify(mockAuditStatSearchRepository, times(1)).save(testAuditStat);
    }

    @Test
    @Transactional
    public void updateNonExistingAuditStat() throws Exception {
        int databaseSizeBeforeUpdate = auditStatRepository.findAll().size();

        // Create the AuditStat
        AuditStatDTO auditStatDTO = auditStatMapper.toDto(auditStat);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAuditStatMockMvc.perform(put("/api/audit-stats")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(auditStatDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AuditStat in the database
        List<AuditStat> auditStatList = auditStatRepository.findAll();
        assertThat(auditStatList).hasSize(databaseSizeBeforeUpdate);

        // Validate the AuditStat in Elasticsearch
        verify(mockAuditStatSearchRepository, times(0)).save(auditStat);
    }

    @Test
    @Transactional
    public void deleteAuditStat() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);

        int databaseSizeBeforeDelete = auditStatRepository.findAll().size();

        // Delete the auditStat
        restAuditStatMockMvc.perform(delete("/api/audit-stats/{id}", auditStat.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<AuditStat> auditStatList = auditStatRepository.findAll();
        assertThat(auditStatList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the AuditStat in Elasticsearch
        verify(mockAuditStatSearchRepository, times(1)).deleteById(auditStat.getId());
    }

    @Test
    @Transactional
    public void searchAuditStat() throws Exception {
        // Initialize the database
        auditStatRepository.saveAndFlush(auditStat);
        when(mockAuditStatSearchRepository.search(queryStringQuery("id:" + auditStat.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(auditStat), PageRequest.of(0, 1), 1));
        // Search the auditStat
        restAuditStatMockMvc.perform(get("/api/_search/audit-stats?query=id:" + auditStat.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(auditStat.getId().intValue())))
            .andExpect(jsonPath("$.[*].runId").value(hasItem(DEFAULT_RUN_ID)))
            .andExpect(jsonPath("$.[*].executiontime").value(hasItem(DEFAULT_EXECUTIONTIME.intValue())))
            .andExpect(jsonPath("$.[*].error").value(hasItem(DEFAULT_ERROR)))
            .andExpect(jsonPath("$.[*].input").value(hasItem(DEFAULT_INPUT)))
            .andExpect(jsonPath("$.[*].output").value(hasItem(DEFAULT_OUTPUT)))
            .andExpect(jsonPath("$.[*].startTime").value(hasItem(DEFAULT_START_TIME.toString())))
            .andExpect(jsonPath("$.[*].endTime").value(hasItem(DEFAULT_END_TIME.toString())))
            .andExpect(jsonPath("$.[*].runconfig").value(hasItem(DEFAULT_RUNCONFIG)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AuditStat.class);
        AuditStat auditStat1 = new AuditStat();
        auditStat1.setId(1L);
        AuditStat auditStat2 = new AuditStat();
        auditStat2.setId(auditStat1.getId());
        assertThat(auditStat1).isEqualTo(auditStat2);
        auditStat2.setId(2L);
        assertThat(auditStat1).isNotEqualTo(auditStat2);
        auditStat1.setId(null);
        assertThat(auditStat1).isNotEqualTo(auditStat2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AuditStatDTO.class);
        AuditStatDTO auditStatDTO1 = new AuditStatDTO();
        auditStatDTO1.setId(1L);
        AuditStatDTO auditStatDTO2 = new AuditStatDTO();
        assertThat(auditStatDTO1).isNotEqualTo(auditStatDTO2);
        auditStatDTO2.setId(auditStatDTO1.getId());
        assertThat(auditStatDTO1).isEqualTo(auditStatDTO2);
        auditStatDTO2.setId(2L);
        assertThat(auditStatDTO1).isNotEqualTo(auditStatDTO2);
        auditStatDTO1.setId(null);
        assertThat(auditStatDTO1).isNotEqualTo(auditStatDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(auditStatMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(auditStatMapper.fromId(null)).isNull();
    }
}
