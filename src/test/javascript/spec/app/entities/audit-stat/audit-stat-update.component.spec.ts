/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { OrderGenTestModule } from '../../../test.module';
import { AuditStatUpdateComponent } from 'app/entities/audit-stat/audit-stat-update.component';
import { AuditStatService } from 'app/entities/audit-stat/audit-stat.service';
import { AuditStat } from 'app/shared/model/audit-stat.model';

describe('Component Tests', () => {
    describe('AuditStat Management Update Component', () => {
        let comp: AuditStatUpdateComponent;
        let fixture: ComponentFixture<AuditStatUpdateComponent>;
        let service: AuditStatService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [OrderGenTestModule],
                declarations: [AuditStatUpdateComponent]
            })
                .overrideTemplate(AuditStatUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(AuditStatUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AuditStatService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new AuditStat(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.auditStat = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new AuditStat();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.auditStat = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
