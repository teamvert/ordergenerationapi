/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { OrderGenTestModule } from '../../../test.module';
import { AuditStatDetailComponent } from 'app/entities/audit-stat/audit-stat-detail.component';
import { AuditStat } from 'app/shared/model/audit-stat.model';

describe('Component Tests', () => {
    describe('AuditStat Management Detail Component', () => {
        let comp: AuditStatDetailComponent;
        let fixture: ComponentFixture<AuditStatDetailComponent>;
        const route = ({ data: of({ auditStat: new AuditStat(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [OrderGenTestModule],
                declarations: [AuditStatDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(AuditStatDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(AuditStatDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.auditStat).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
