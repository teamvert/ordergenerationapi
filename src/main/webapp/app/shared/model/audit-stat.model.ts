import { Moment } from 'moment';

export interface IAuditStat {
    id?: number;
    runId?: string;
    executiontime?: number;
    error?: string;
    input?: string;
    output?: string;
    startTime?: Moment;
    endTime?: Moment;
    runconfig?: string;
}

export class AuditStat implements IAuditStat {
    constructor(
        public id?: number,
        public runId?: string,
        public executiontime?: number,
        public error?: string,
        public input?: string,
        public output?: string,
        public startTime?: Moment,
        public endTime?: Moment,
        public runconfig?: string
    ) {}
}
