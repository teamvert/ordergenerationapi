import { NgModule } from '@angular/core';

import { OrderGenSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [OrderGenSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [OrderGenSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class OrderGenSharedCommonModule {}
