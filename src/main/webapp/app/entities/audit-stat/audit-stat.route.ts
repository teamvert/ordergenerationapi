import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { AuditStat } from 'app/shared/model/audit-stat.model';
import { AuditStatService } from './audit-stat.service';
import { AuditStatComponent } from './audit-stat.component';
import { AuditStatDetailComponent } from './audit-stat-detail.component';
import { AuditStatUpdateComponent } from './audit-stat-update.component';
import { AuditStatDeletePopupComponent } from './audit-stat-delete-dialog.component';
import { IAuditStat } from 'app/shared/model/audit-stat.model';

@Injectable({ providedIn: 'root' })
export class AuditStatResolve implements Resolve<IAuditStat> {
    constructor(private service: AuditStatService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IAuditStat> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<AuditStat>) => response.ok),
                map((auditStat: HttpResponse<AuditStat>) => auditStat.body)
            );
        }
        return of(new AuditStat());
    }
}

export const auditStatRoute: Routes = [
    {
        path: '',
        component: AuditStatComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'AuditStats'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: AuditStatDetailComponent,
        resolve: {
            auditStat: AuditStatResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'AuditStats'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: AuditStatUpdateComponent,
        resolve: {
            auditStat: AuditStatResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'AuditStats'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: AuditStatUpdateComponent,
        resolve: {
            auditStat: AuditStatResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'AuditStats'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const auditStatPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: AuditStatDeletePopupComponent,
        resolve: {
            auditStat: AuditStatResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'AuditStats'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
