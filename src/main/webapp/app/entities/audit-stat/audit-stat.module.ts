import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { OrderGenSharedModule } from 'app/shared';
import {
    AuditStatComponent,
    AuditStatDetailComponent,
    AuditStatUpdateComponent,
    AuditStatDeletePopupComponent,
    AuditStatDeleteDialogComponent,
    auditStatRoute,
    auditStatPopupRoute
} from './';

const ENTITY_STATES = [...auditStatRoute, ...auditStatPopupRoute];

@NgModule({
    imports: [OrderGenSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        AuditStatComponent,
        AuditStatDetailComponent,
        AuditStatUpdateComponent,
        AuditStatDeleteDialogComponent,
        AuditStatDeletePopupComponent
    ],
    entryComponents: [AuditStatComponent, AuditStatUpdateComponent, AuditStatDeleteDialogComponent, AuditStatDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OrderGenAuditStatModule {}
