import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { IAuditStat } from 'app/shared/model/audit-stat.model';
import { AuditStatService } from './audit-stat.service';

@Component({
    selector: 'jhi-audit-stat-update',
    templateUrl: './audit-stat-update.component.html'
})
export class AuditStatUpdateComponent implements OnInit {
    auditStat: IAuditStat;
    isSaving: boolean;
    startTime: string;
    endTime: string;

    constructor(protected auditStatService: AuditStatService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ auditStat }) => {
            this.auditStat = auditStat;
            this.startTime = this.auditStat.startTime != null ? this.auditStat.startTime.format(DATE_TIME_FORMAT) : null;
            this.endTime = this.auditStat.endTime != null ? this.auditStat.endTime.format(DATE_TIME_FORMAT) : null;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.auditStat.startTime = this.startTime != null ? moment(this.startTime, DATE_TIME_FORMAT) : null;
        this.auditStat.endTime = this.endTime != null ? moment(this.endTime, DATE_TIME_FORMAT) : null;
        if (this.auditStat.id !== undefined) {
            this.subscribeToSaveResponse(this.auditStatService.update(this.auditStat));
        } else {
            this.subscribeToSaveResponse(this.auditStatService.create(this.auditStat));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IAuditStat>>) {
        result.subscribe((res: HttpResponse<IAuditStat>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
