import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAuditStat } from 'app/shared/model/audit-stat.model';

@Component({
    selector: 'jhi-audit-stat-detail',
    templateUrl: './audit-stat-detail.component.html'
})
export class AuditStatDetailComponent implements OnInit {
    auditStat: IAuditStat;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ auditStat }) => {
            this.auditStat = auditStat;
        });
    }

    previousState() {
        window.history.back();
    }
}
