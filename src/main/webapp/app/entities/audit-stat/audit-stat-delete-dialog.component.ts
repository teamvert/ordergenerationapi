import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAuditStat } from 'app/shared/model/audit-stat.model';
import { AuditStatService } from './audit-stat.service';

@Component({
    selector: 'jhi-audit-stat-delete-dialog',
    templateUrl: './audit-stat-delete-dialog.component.html'
})
export class AuditStatDeleteDialogComponent {
    auditStat: IAuditStat;

    constructor(
        protected auditStatService: AuditStatService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.auditStatService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'auditStatListModification',
                content: 'Deleted an auditStat'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-audit-stat-delete-popup',
    template: ''
})
export class AuditStatDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ auditStat }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(AuditStatDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.auditStat = auditStat;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/audit-stat', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/audit-stat', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
