import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IAuditStat } from 'app/shared/model/audit-stat.model';

type EntityResponseType = HttpResponse<IAuditStat>;
type EntityArrayResponseType = HttpResponse<IAuditStat[]>;

@Injectable({ providedIn: 'root' })
export class AuditStatService {
    public resourceUrl = SERVER_API_URL + 'api/audit-stats';
    public resourceSearchUrl = SERVER_API_URL + 'api/_search/audit-stats';

    constructor(protected http: HttpClient) {}

    create(auditStat: IAuditStat): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(auditStat);
        return this.http
            .post<IAuditStat>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(auditStat: IAuditStat): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(auditStat);
        return this.http
            .put<IAuditStat>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IAuditStat>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IAuditStat[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IAuditStat[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    protected convertDateFromClient(auditStat: IAuditStat): IAuditStat {
        const copy: IAuditStat = Object.assign({}, auditStat, {
            startTime: auditStat.startTime != null && auditStat.startTime.isValid() ? auditStat.startTime.toJSON() : null,
            endTime: auditStat.endTime != null && auditStat.endTime.isValid() ? auditStat.endTime.toJSON() : null
        });
        return copy;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.startTime = res.body.startTime != null ? moment(res.body.startTime) : null;
            res.body.endTime = res.body.endTime != null ? moment(res.body.endTime) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((auditStat: IAuditStat) => {
                auditStat.startTime = auditStat.startTime != null ? moment(auditStat.startTime) : null;
                auditStat.endTime = auditStat.endTime != null ? moment(auditStat.endTime) : null;
            });
        }
        return res;
    }
}
