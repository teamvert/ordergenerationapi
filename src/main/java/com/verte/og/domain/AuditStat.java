package com.verte.og.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A AuditStat.
 */
@Entity
@Table(name = "audit_stat")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "auditstat")
public class AuditStat implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 100)
    @Column(name = "run_id", length = 100)
    private String runId;

    @Column(name = "executiontime")
    private Long executiontime;

    @Size(max = 1000)
    @Column(name = "error", length = 1000)
    private String error;

    @Size(max = 1000)
    @Column(name = "input", length = 1000)
    private String input;

    @Size(max = 1000)
    @Column(name = "output", length = 1000)
    private String output;

    @Column(name = "start_time")
    private Instant startTime;

    @Column(name = "end_time")
    private Instant endTime;

    @Size(max = 10000)
    @Column(name = "runconfig", length = 10000)
    private String runconfig;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRunId() {
        return runId;
    }

    public AuditStat runId(String runId) {
        this.runId = runId;
        return this;
    }

    public void setRunId(String runId) {
        this.runId = runId;
    }

    public Long getExecutiontime() {
        return executiontime;
    }

    public AuditStat executiontime(Long executiontime) {
        this.executiontime = executiontime;
        return this;
    }

    public void setExecutiontime(Long executiontime) {
        this.executiontime = executiontime;
    }

    public String getError() {
        return error;
    }

    public AuditStat error(String error) {
        this.error = error;
        return this;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getInput() {
        return input;
    }

    public AuditStat input(String input) {
        this.input = input;
        return this;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getOutput() {
        return output;
    }

    public AuditStat output(String output) {
        this.output = output;
        return this;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public AuditStat startTime(Instant startTime) {
        this.startTime = startTime;
        return this;
    }

    public void setStartTime(Instant startTime) {
        this.startTime = startTime;
    }

    public Instant getEndTime() {
        return endTime;
    }

    public AuditStat endTime(Instant endTime) {
        this.endTime = endTime;
        return this;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }

    public String getRunconfig() {
        return runconfig;
    }

    public AuditStat runconfig(String runconfig) {
        this.runconfig = runconfig;
        return this;
    }

    public void setRunconfig(String runconfig) {
        this.runconfig = runconfig;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AuditStat auditStat = (AuditStat) o;
        if (auditStat.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), auditStat.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AuditStat{" +
            "id=" + getId() +
            ", runId='" + getRunId() + "'" +
            ", executiontime=" + getExecutiontime() +
            ", error='" + getError() + "'" +
            ", input='" + getInput() + "'" +
            ", output='" + getOutput() + "'" +
            ", startTime='" + getStartTime() + "'" +
            ", endTime='" + getEndTime() + "'" +
            ", runconfig='" + getRunconfig() + "'" +
            "}";
    }
}
