/**
 * View Models used by Spring MVC REST controllers.
 */
package com.verte.og.web.rest.vm;
