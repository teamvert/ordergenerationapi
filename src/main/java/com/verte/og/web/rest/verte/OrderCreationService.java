package com.verte.og.web.rest.verte;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.scheduling.annotation.Async;


import com.verte.og.domain.AuditStat;
import com.verte.og.service.AuditStatService;
import com.verte.og.service.UserService;
import com.verte.og.service.dto.AuditStatDTO;

@Service
public class OrderCreationService {
	
	 private final Logger log = LoggerFactory.getLogger(OrderCreationService.class);

	private String NOP_URL = "https://demostore4.projectverte.com/api";

	private String NOP_Username = "orders@store.com";

	private String NOP_Password = "Verte@2019";

	private String NOP_GiftCard = "7bb4d23e-f956";

	private String NOP_VendorPrefix = "VS0025";
	
	private String FOLDER = "C:\\ogs";
	
	@Autowired
	private AuditStatService auditStatService;
	

	public ArrayList<MyPair[]> generate(String runid, OrderGenVM ordGenVM) throws Exception {

		HashMap<Integer, Integer> batchSize = new HashMap();
		batchSize.put(Integer.valueOf("0"), ordGenVM.getOrderCtr());
		ArrayList<MyPair[]> myPairList = PrepData.generate(1, ordGenVM.getOrderSeq(), 1,
				new ArrayList<MyPair>(Arrays.asList(ordGenVM.getItemQtyArray())), batchSize, ordGenVM.getpL1Q1(),
				ordGenVM.getpL1Q2(), ordGenVM.getpL2Q1(), ordGenVM.getpL3Q1(), ordGenVM.getpL4Q1(),
				FOLDER + File.separator  + runid);
       return myPairList;
		 
	}

	@Async
	public void sendRestOrder(String runid, OrderGenVM ordGenVM, MyPair[] mypair) throws Exception {
			
			String error = null;
			String output = null ;
			Instant startTime = Instant.now();
			Instant endTime =null;
			
			try {
				output = createOrder(mypair).toString();
			}catch(Exception ex) {
				error = ex.getMessage();
			}
			
			endTime = Instant.now();
			String threadId = Thread.currentThread().getName().split("-")[1];
			AuditStatDTO auditStatDTO = new AuditStatDTO(null,runid,
					                  endTime.toEpochMilli()-startTime.toEpochMilli(),
					                   error,Arrays.toString(mypair), output, startTime,
					      			  endTime,"Threadid "+threadId+" "+ordGenVM.toString()
					                  );
			auditStatService.save(auditStatDTO);
		
	}

	
	
	@PostMapping("/getToken")
	public String getAuthToken(String emailid) throws Exception {

		RestTemplate restTemplate = new RestTemplate();
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		Map map = new HashMap<String, String>();
		map.put("Content-Type", "application/json");

		Map req_payload = new HashMap();
		req_payload.put("Username", emailid);
		req_payload.put("Password", NOP_Password);

		HttpEntity<?> request = new HttpEntity<>(req_payload, headers);
		String url = NOP_URL + "/VerteAuth/Authenticate";

		ResponseEntity<?> response = new RestTemplate().postForEntity(url, request, String.class);
		JSONObject obj = new JSONObject(response.getBody().toString());
		String token = obj.getString("Token");

		return token;
	}

	@PostMapping("/createOrder")
	public JSONObject createOrder(@RequestBody MyPair[] orderDetail) throws Exception {
		
		String threadId = Thread.currentThread().getName().split("-")[1];
		
		String email = "ordersplacement" + threadId +"@yourstore.com";

		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json");
		headers.set("Authorization", "Bearer " + getAuthToken(email));

		List orderItemsArg = new ArrayList<>();

		for (int i = 0; i < orderDetail.length; i++) {

			LinkedHashMap orderItemArg = new LinkedHashMap<>();

			MyPair order = orderDetail[i];

			orderItemArg.put("sku", order.getItem());
			orderItemArg.put("vendorPrefix", NOP_VendorPrefix);
			orderItemArg.put("quantity", order.getQty());

			orderItemsArg.add(orderItemArg);
		}

		LinkedHashMap<String, Object> addressArg = new LinkedHashMap<>();

		addressArg.put("countryCode", "US");
		addressArg.put("stateCode", "GA");
		addressArg.put("city", "Sandy Springs");
		addressArg.put("address1", "8601 Dunwoody Place, Ste 200");
		addressArg.put("zipPostalCode", "30350");
		addressArg.put("phone", "5555555555");
		addressArg.put("firstName", "Orders"+threadId);
		addressArg.put("lastName", "Placement"+threadId);

		Map argument = new LinkedHashMap<String, Object>();

		argument.put("email", email);
		argument.put("giftCard", NOP_GiftCard);
		argument.put("orderItems", orderItemsArg);
		argument.put("address", addressArg);
		JSONObject obj;

		try {

			HttpEntity<?> request = new HttpEntity(argument, headers);
			String url = NOP_URL + "/VerteSimulation/CreateOrder";

			ResponseEntity<?> response = new RestTemplate().postForEntity(url, request, String.class);
			obj = new JSONObject(response.getBody().toString());
			// String token = obj.getString("Token");

		} catch (Throwable e) {
			throw new Exception(e.getMessage());
		}

		return obj;
	}

}

class PrepData {
	/*
	 * public static void main(String[] args) { Scanner reader = new
	 * Scanner(System.in);
	 * 
	 * System.out.println("No of batches: "); int noOfBatch = reader.nextInt();
	 * 
	 * System.out.
	 * println("Order number seq (e.g. ORD_20180726. Will append a running counter as ORD_20180726_xxxx): "
	 * ); String orderSeq = reader.next(); int orderCtr = 1;
	 * 
	 * System.out.
	 * println("Enter Inventory file path (csv format - Item, Quantity): "); String
	 * invFileName = reader.next();
	 * 
	 * FileToArray fta = new FileToArray(); ArrayList<MyPair> itemQtyArray =
	 * fta.CSVToArray(invFileName);
	 * 
	 * HashMap<Integer, Integer> batchSize = new HashMap(); for (int i = 0; i <
	 * noOfBatch; i++) { System.out.println("No of Orders for Batch " + (i + 1));
	 * int n = reader.nextInt(); batchSize.put(Integer.valueOf(i),
	 * Integer.valueOf(n)); } System.out.println("% of 1 line 1 unit orders: "); int
	 * pL1Q1 = reader.nextInt();
	 * 
	 * System.out.println("% of 1 line 2 unit orders: "); int pL1Q2 =
	 * reader.nextInt();
	 * 
	 * System.out.println("% of 2 line 1 unit orders: "); int pL2Q1 =
	 * reader.nextInt();
	 * 
	 * System.out.println("% of 3 line 1 unit orders: "); int pL3Q1 =
	 * reader.nextInt();
	 * 
	 * System.out.println("% of 4 line 1 unit orders: "); int pL4Q1 =
	 * reader.nextInt();
	 * 
	 * System.out.println("Path to create batch files (Path should already exist): "
	 * ); String path = reader.next();
	 * 
	 * reader.close();
	 * 
	 * generate(noOfBatch, orderSeq, orderCtr, itemQtyArray, batchSize, pL1Q1,
	 * pL1Q2, pL2Q1, pL3Q1, pL4Q1, path);
	 * 
	 * }
	 */
	static ArrayList<MyPair[]> generate(int noOfBatch, String orderSeq, int orderCtr, ArrayList<MyPair> itemQtyArray,
			HashMap<Integer, Integer> batchSize, int pL1Q1, int pL1Q2, int pL2Q1, int pL3Q1, int pL4Q1, String path) {

		int itemCtr = 0;
		ArrayList<MyPair[]> mypairList = new ArrayList();

		for (int batch = 0; batch < noOfBatch; batch++) {
			String batchFolder = path + File.separator +"batch" + String.valueOf(batch);
			//File dir = new File(batchFolder);
			//dir.mkdirs();

			int noOfOrd = ((Integer) batchSize.get(Integer.valueOf(batch))).intValue();
			int nL1Q1 = (int) Math.ceil(pL1Q1 * noOfOrd / 100);
			int nL1Q2 = (int) Math.ceil(pL1Q2 * noOfOrd / 100);
			int nL2Q1 = (int) Math.ceil(pL2Q1 * noOfOrd / 100);
			int nL3Q1 = (int) Math.ceil(pL3Q1 * noOfOrd / 100);
			int nL4Q1 = (int) Math.ceil(pL4Q1 * noOfOrd / 100);
		//	try {
				//String fname = batchFolder + File.separator +"l1q1.csv";
			//	FileWriter writer = new FileWriter(fname, true);
			//	writer.write(
			//			"Items.Sku,Items.Quantity ,Items.UnitPrice,Items.TaxPrice,Items.ShippingPrice,Items.ShippingTaxPrice\n");

				String ordFileName = batchFolder + File.separator +"OrderIDs_l1q1.csv";
			//	FileWriter ordWriter = new FileWriter(ordFileName, true);
			//	ordWriter.write("SiteOrderID\n");
				for (int j = 0; (j < nL1Q1) && (itemQtyArray.size() > 0); j++) {
					MyPair pair = (MyPair) itemQtyArray.get(itemCtr);
					pair.decrementQty();
					if (pair.getQty().intValue() <= 0) {
						itemQtyArray.remove(itemCtr);
					} else {
						itemQtyArray.set(itemCtr, pair);
						itemCtr++;
					}
					if (itemCtr >= itemQtyArray.size()) {
						itemCtr = 0;
					}
					String csv = String.format("%s,1,0.1,0,0,0\n", new Object[] { pair.getItem() });
			//		writer.write(csv);
					String orderNo = String.format("%s_%04d\n", new Object[] { orderSeq, Integer.valueOf(orderCtr++) });
			//		ordWriter.write(orderNo);
					mypairList.add(new MyPair[] { new MyPair(pair.getItem(), 1) });
				}
			//	writer.close();
			//	ordWriter.close();
		//	} catch (IOException localIOException) {
		//	}
		//	try {
		//		String fname = batchFolder + File.separator +"l1q2.csv";
			//	FileWriter writer = new FileWriter(fname, true);

		//		String ordFileName = batchFolder + File.separator +"OrderIDs_l1q2.csv";
			//	FileWriter ordWriter = new FileWriter(ordFileName, true);
			//	ordWriter.write("SiteOrderID\n");

			//	writer.write(
			//			"Items.Sku1,Items.Quantity,Items.UnitPrice,Items.TotalPrice,Items.TaxPrice,Items.ShippingPrice,Items.ShippingTaxPrice\n");
				for (int j = 0; (j < nL1Q2) && (itemQtyArray.size() > 0); j++) {
					MyPair pair = (MyPair) itemQtyArray.get(itemCtr);
					if (pair.getQty().intValue() < 2) {
						itemCtr++;
						j--;
					} else {
						pair.decrementQty();
						pair.decrementQty();
						if (pair.getQty().intValue() <= 0) {
							itemQtyArray.remove(itemCtr);
						} else {
							itemQtyArray.set(itemCtr, pair);
							itemCtr++;
						}
						if (itemCtr >= itemQtyArray.size()) {
							itemCtr = 0;
						}
						String csv = String.format("%s,2,0.1,0.2,0,0,0\n", new Object[] { pair.getItem() });
			//			writer.write(csv);

						String orderNo = String.format("%s_%04d\n",
								new Object[] { orderSeq, Integer.valueOf(orderCtr++) });
			//			ordWriter.write(orderNo);
						mypairList.add(new MyPair[] { new MyPair(pair.getItem(), 2) });
					}
				}
			//	writer.close();
			//	ordWriter.close();
		//	} catch (IOException localIOException1) {
		//	}
		//	try {
		//		String fname = batchFolder + File.separator + "l2q1.csv";
			//	FileWriter writer = new FileWriter(fname, true);

		//		String ordFileName = batchFolder + File.separator + "OrderIDs_l2q1.csv";
			//	FileWriter ordWriter = new FileWriter(ordFileName, true);
			//	ordWriter.write("SiteOrderID\n");

			//	writer.write(
			//			"Items.Sku1,Items.Quantity1,Items.UnitPrice1,Items.Sku2,Items.Quantity2 ,Items.UnitPrice2,Items.TotalPrice,Items.TaxPrice,Items.ShippingPrice,Items.ShippingTaxPrice\n");
				for (int j = 0; (j < nL2Q1) && (itemQtyArray.size() > 0); j++) {
					MyPair pair1 = (MyPair) itemQtyArray.get(itemCtr);
					pair1.decrementQty();
					if (pair1.getQty().intValue() <= 0) {
						itemQtyArray.remove(itemCtr);
					} else {
						itemQtyArray.set(itemCtr, pair1);
						itemCtr++;
					}
					if (itemCtr >= itemQtyArray.size()) {
						itemCtr = 0;
					}
					MyPair pair2 = (MyPair) itemQtyArray.get(itemCtr);
					pair2.decrementQty();
					if (pair2.getQty().intValue() <= 0) {
						itemQtyArray.remove(itemCtr);
					} else {
						itemQtyArray.set(itemCtr, pair2);
						itemCtr++;
					}
					if (itemCtr >= itemQtyArray.size()) {
						itemCtr = 0;
					}
					String csv = String.format("%s,1,0.1,%s,1,0.1,0.2,0,0,0\n",
							new Object[] { pair1.getItem(), pair2.getItem() });
			//		writer.write(csv);
					String orderNo = String.format("%s_%04d\n", new Object[] { orderSeq, Integer.valueOf(orderCtr++) });
			///		ordWriter.write(orderNo);
					mypairList.add(new MyPair[] { new MyPair(pair1.getItem(), 1), new MyPair(pair2.getItem(), 1) });
				}
			//	writer.close();
			//	ordWriter.close();
		//	} catch (IOException localIOException2) {
		//	}
		//	try {
		//		String fname = batchFolder + File.separator + "l3q1.csv";
			//	FileWriter writer = new FileWriter(fname, true);

		//		String ordFileName = batchFolder + File.separator + "OrderIDs_l3q1.csv";
			//	FileWriter ordWriter = new FileWriter(ordFileName, true);
			//	ordWriter.write("SiteOrderID\n");

			//	writer.write(
			//			"Items.Sku1,Items.Quantity1,Items.UnitPrice1,Items.Sku2,Items.Quantity2 ,Items.UnitPrice2,Items.Sku3,Items.Quantity3,Items.UnitPrice3,Items.TotalPrice,Items.TaxPrice,Items.ShippingPrice,Items.ShippingTaxPrice\n");
				for (int j = 0; (j < nL3Q1) && (itemQtyArray.size() > 0); j++) {
					MyPair pair1 = (MyPair) itemQtyArray.get(itemCtr);
					pair1.decrementQty();
					if (pair1.getQty().intValue() <= 0) {
						itemQtyArray.remove(itemCtr);
					} else {
						itemQtyArray.set(itemCtr, pair1);
						itemCtr++;
					}
					if (itemCtr >= itemQtyArray.size()) {
						itemCtr = 0;
					}
					MyPair pair2 = (MyPair) itemQtyArray.get(itemCtr);
					pair2.decrementQty();
					if (pair2.getQty().intValue() <= 0) {
						itemQtyArray.remove(itemCtr);
					} else {
						itemQtyArray.set(itemCtr, pair2);
						itemCtr++;
					}
					if (itemCtr >= itemQtyArray.size()) {
						itemCtr = 0;
					}
					MyPair pair3 = (MyPair) itemQtyArray.get(itemCtr);
					pair3.decrementQty();
					if (pair3.getQty().intValue() <= 0) {
						itemQtyArray.remove(itemCtr);
					} else {
						itemQtyArray.set(itemCtr, pair3);
						itemCtr++;
					}
					if (itemCtr >= itemQtyArray.size()) {
						itemCtr = 0;
					}
					String csv = String.format("%s,1,0.1,%s,1,0.1,%s,1,0.1,0.3,0,0,0\n",
							new Object[] { pair1.getItem(), pair2.getItem(), pair3.getItem() });
			//		writer.write(csv);
					String orderNo = String.format("%s_%04d\n", new Object[] { orderSeq, Integer.valueOf(orderCtr++) });
			//		ordWriter.write(orderNo);
					mypairList.add(new MyPair[] { new MyPair(pair1.getItem(), 1), new MyPair(pair2.getItem(), 1),
							new MyPair(pair3.getItem(), 1) });
				}
			//	writer.close();
			//	ordWriter.close();
		//	} catch (IOException localIOException3) {
		//	}
		//	try {
		//		String fname = batchFolder + File.separator + "l4q1.csv";
			//	FileWriter writer = new FileWriter(fname, true);

		//		String ordFileName = batchFolder + File.separator + "OrderIDs_l4q1.csv";
			//	FileWriter ordWriter = new FileWriter(ordFileName, true);
			//	ordWriter.write("SiteOrderID\n");

			//	writer.write(
			//			"Items.Sku1,Items.Quantity1,Items.UnitPrice1,Items.Sku2,Items.Quantity2 ,Items.UnitPrice2,Items.Sku3,Items.Quantity3,Items.UnitPrice3,Items.Sku4,Items.Quantity4,Items.UnitPrice4,Items.TotalPrice,Items.TaxPrice,Items.ShippingPrice,Items.ShippingTaxPrice\n");
				for (int j = 0; (j < nL4Q1) && (itemQtyArray.size() > 0); j++) {
					MyPair pair1 = (MyPair) itemQtyArray.get(itemCtr);
					pair1.decrementQty();
					if (pair1.getQty().intValue() <= 0) {
						itemQtyArray.remove(itemCtr);
					} else {
						itemQtyArray.set(itemCtr, pair1);
						itemCtr++;
					}
					if (itemCtr >= itemQtyArray.size()) {
						itemCtr = 0;
					}
					MyPair pair2 = (MyPair) itemQtyArray.get(itemCtr);
					pair2.decrementQty();
					if (pair2.getQty().intValue() <= 0) {
						itemQtyArray.remove(itemCtr);
					} else {
						itemQtyArray.set(itemCtr, pair2);
						itemCtr++;
					}
					if (itemCtr >= itemQtyArray.size()) {
						itemCtr = 0;
					}
					MyPair pair3 = (MyPair) itemQtyArray.get(itemCtr);
					pair3.decrementQty();
					if (pair3.getQty().intValue() <= 0) {
						itemQtyArray.remove(itemCtr);
					} else {
						itemQtyArray.set(itemCtr, pair3);
						itemCtr++;
					}
					if (itemCtr >= itemQtyArray.size()) {
						itemCtr = 0;
					}
					MyPair pair4 = (MyPair) itemQtyArray.get(itemCtr);
					pair4.decrementQty();
					if (pair4.getQty().intValue() <= 0) {
						itemQtyArray.remove(itemCtr);
					} else {
						itemQtyArray.set(itemCtr, pair4);
						itemCtr++;
					}
					if (itemCtr >= itemQtyArray.size()) {
						itemCtr = 0;
					}
					String csv = String.format("%s,1,0.1,%s,1,0.1,%s,1,0.1,%s,1,0.1,0.4,0,0,0\n",
							new Object[] { pair1.getItem(), pair2.getItem(), pair3.getItem(), pair4.getItem() });
			//		writer.write(csv);
					String orderNo = String.format("%s_%04d\n", new Object[] { orderSeq, Integer.valueOf(orderCtr++) });
			//		ordWriter.write(orderNo);
					mypairList.add(new MyPair[] { new MyPair(pair1.getItem(), 1), new MyPair(pair2.getItem(), 1),
							new MyPair(pair3.getItem(), 1), new MyPair(pair4.getItem(), 1) });

				}
			//	writer.close();
			//	ordWriter.close();
		//	} catch (IOException localIOException4) {
		//	}
		}
		Dump(path, itemQtyArray);
		return mypairList;
	}

	public static void Dump(String path, ArrayList<MyPair> itemQtyArray) {
		//try {
			String fname = path + File.separator +"RemainingInv.csv";
			//FileWriter writer = new FileWriter(fname, true);
			for (int j = 0; j < itemQtyArray.size(); j++) {
				MyPair pair = (MyPair) itemQtyArray.get(j);
				String csv = String.format("%s,%d\n", new Object[] { pair.getItem(), pair.getQty() });
			//	writer.write(csv);
			}
			//writer.close();
		//} catch (IOException localIOException) {
		//}
	}

}

class MyPair {
	
	private String item;
	private Integer qty;

	public MyPair() {

	}

	public MyPair(String aItem, Integer aQty) {
		this.item = aItem;
		this.qty = aQty;
	}

	public String getItem() {
		return this.item;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public Integer getQty() {
		return this.qty;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public void decrementQty() {
		this.qty = Integer.valueOf(this.qty.intValue() - 1);
	}
	@Override
	public String toString() {
		return "Order [item=" + item + ", qty=" + qty + "]";
	}

}

class FileToArray {
	public ArrayList<MyPair> CSVToArray(String invFileName) {
		File file = new File(invFileName);

		ArrayList<MyPair> itemQty = new ArrayList();
		try {
			Scanner inputStream = new Scanner(file);
			while (inputStream.hasNext()) {
				String data = inputStream.next();
				String[] values = data.split(",");
				Integer qty = Integer.valueOf(Integer.parseInt(values[1]));
				MyPair pair = new MyPair(values[0], qty);
				itemQty.add(pair);
			}
			inputStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Collections.shuffle(itemQty);
		return itemQty;
	}
}

class OrderGenVM {

	
	int noOfBatch;
	String orderSeq;
	int orderCtr;
	MyPair[] itemQtyArray;
	HashMap<Integer, Integer> batchSize;
	int pL1Q1;
	int pL1Q2;
	int pL2Q1;
	int pL3Q1;
	int pL4Q1;
	String path;

	public int getNoOfBatch() {
		return noOfBatch;
	}

	public void setNoOfBatch(int noOfBatch) {
		this.noOfBatch = noOfBatch;
	}

	public String getOrderSeq() {
		return orderSeq;
	}

	public void setOrderSeq(String orderSeq) {
		this.orderSeq = orderSeq;
	}

	public int getOrderCtr() {
		return orderCtr;
	}

	public void setOrderCtr(int orderCtr) {
		this.orderCtr = orderCtr;
	}

	public MyPair[] getItemQtyArray() {
		return itemQtyArray;
	}

	public void setItemQtyArray(MyPair[] itemQtyArray) {
		this.itemQtyArray = itemQtyArray;
	}

	public HashMap<Integer, Integer> getBatchSize() {
		return batchSize;
	}

	public void setBatchSize(HashMap<Integer, Integer> batchSize) {
		this.batchSize = batchSize;
	}

	public int getpL1Q1() {
		return pL1Q1;
	}

	public void setpL1Q1(int pL1Q1) {
		this.pL1Q1 = pL1Q1;
	}

	public int getpL1Q2() {
		return pL1Q2;
	}

	public void setpL1Q2(int pL1Q2) {
		this.pL1Q2 = pL1Q2;
	}

	public int getpL2Q1() {
		return pL2Q1;
	}

	public void setpL2Q1(int pL2Q1) {
		this.pL2Q1 = pL2Q1;
	}

	public int getpL3Q1() {
		return pL3Q1;
	}

	public void setpL3Q1(int pL3Q1) {
		this.pL3Q1 = pL3Q1;
	}

	public int getpL4Q1() {
		return pL4Q1;
	}

	public void setpL4Q1(int pL4Q1) {
		this.pL4Q1 = pL4Q1;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
	@Override
	public String toString() {
		return "OrderGenVM [noOfBatch=" + noOfBatch + ", orderSeq=" + orderSeq + ", orderCtr=" + orderCtr
				+ ", itemQtyArray=" + Arrays.toString(itemQtyArray) + ", batchSize=" + batchSize + ", pL1Q1=" + pL1Q1
				+ ", pL1Q2=" + pL1Q2 + ", pL2Q1=" + pL2Q1 + ", pL3Q1=" + pL3Q1 + ", pL4Q1=" + pL4Q1 + ", path=" + path
				+ "]";
	}

}

class FileToREST {
	public ArrayList<MyPair> CSVToREST(String invFileName) {
		File file = new File(invFileName);

		ArrayList<MyPair> itemQty = new ArrayList();
		try {
			Scanner inputStream = new Scanner(file);
			while (inputStream.hasNext()) {
				String data = inputStream.next();
				String[] values = data.split(",");
				Integer qty = Integer.valueOf(Integer.parseInt(values[1]));
				MyPair pair = new MyPair(values[0], qty);
				itemQty.add(pair);
			}
			inputStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Collections.shuffle(itemQty);
		return itemQty;
	}
}

/*
class AuditStat{
	
	MyPair[] myPair;
	long executiontime;
	String error;
	String output;
	Date startTime;
	Date endTime;
	
	public AuditStat(){
		
	}
	
	public AuditStat(MyPair[] myPair, long executiontime, String error, String output, Date startTime, Date endTime) {
		super();
		this.myPair = myPair;
		this.executiontime = executiontime;
		this.error = error;
		this.output = output;
		this.startTime = startTime;
		this.endTime = endTime;
	}
	
	public MyPair[] getMyPair() {
		return myPair;
	}
	public void setMyPair(MyPair[] myPair) {
		this.myPair = myPair;
	}
	public long getExecutiontime() {
		return executiontime;
	}
	public void setExecutiontime(long executiontime) {
		this.executiontime = executiontime;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getOutput() {
		return output;
	}
	public void setOutput(String output) {
		this.output = output;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

}*/
