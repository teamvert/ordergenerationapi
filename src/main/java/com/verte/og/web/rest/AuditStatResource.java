package com.verte.og.web.rest;
import com.verte.og.service.AuditStatService;
import com.verte.og.web.rest.errors.BadRequestAlertException;
import com.verte.og.web.rest.util.HeaderUtil;
import com.verte.og.web.rest.util.PaginationUtil;
import com.verte.og.service.dto.AuditStatDTO;
import com.verte.og.service.dto.AuditStatCriteria;
import com.verte.og.service.AuditStatQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AuditStat.
 */
@RestController
@RequestMapping("/api")
public class AuditStatResource {

    private final Logger log = LoggerFactory.getLogger(AuditStatResource.class);

    private static final String ENTITY_NAME = "auditStat";

    private final AuditStatService auditStatService;

    private final AuditStatQueryService auditStatQueryService;

    public AuditStatResource(AuditStatService auditStatService, AuditStatQueryService auditStatQueryService) {
        this.auditStatService = auditStatService;
        this.auditStatQueryService = auditStatQueryService;
    }

    /**
     * POST  /audit-stats : Create a new auditStat.
     *
     * @param auditStatDTO the auditStatDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new auditStatDTO, or with status 400 (Bad Request) if the auditStat has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/audit-stats")
    public ResponseEntity<AuditStatDTO> createAuditStat(@Valid @RequestBody AuditStatDTO auditStatDTO) throws URISyntaxException {
        log.debug("REST request to save AuditStat : {}", auditStatDTO);
        if (auditStatDTO.getId() != null) {
            throw new BadRequestAlertException("A new auditStat cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AuditStatDTO result = auditStatService.save(auditStatDTO);
        return ResponseEntity.created(new URI("/api/audit-stats/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /audit-stats : Updates an existing auditStat.
     *
     * @param auditStatDTO the auditStatDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated auditStatDTO,
     * or with status 400 (Bad Request) if the auditStatDTO is not valid,
     * or with status 500 (Internal Server Error) if the auditStatDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/audit-stats")
    public ResponseEntity<AuditStatDTO> updateAuditStat(@Valid @RequestBody AuditStatDTO auditStatDTO) throws URISyntaxException {
        log.debug("REST request to update AuditStat : {}", auditStatDTO);
        if (auditStatDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AuditStatDTO result = auditStatService.save(auditStatDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, auditStatDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /audit-stats : get all the auditStats.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of auditStats in body
     */
    @GetMapping("/audit-stats")
    public ResponseEntity<List<AuditStatDTO>> getAllAuditStats(AuditStatCriteria criteria, Pageable pageable) {
        log.debug("REST request to get AuditStats by criteria: {}", criteria);
        Page<AuditStatDTO> page = auditStatQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/audit-stats");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * GET  /audit-stats/count : count all the auditStats.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
    @GetMapping("/audit-stats/count")
    public ResponseEntity<Long> countAuditStats(AuditStatCriteria criteria) {
        log.debug("REST request to count AuditStats by criteria: {}", criteria);
        return ResponseEntity.ok().body(auditStatQueryService.countByCriteria(criteria));
    }

    /**
     * GET  /audit-stats/:id : get the "id" auditStat.
     *
     * @param id the id of the auditStatDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the auditStatDTO, or with status 404 (Not Found)
     */
    @GetMapping("/audit-stats/{id}")
    public ResponseEntity<AuditStatDTO> getAuditStat(@PathVariable Long id) {
        log.debug("REST request to get AuditStat : {}", id);
        Optional<AuditStatDTO> auditStatDTO = auditStatService.findOne(id);
        return ResponseUtil.wrapOrNotFound(auditStatDTO);
    }

    /**
     * DELETE  /audit-stats/:id : delete the "id" auditStat.
     *
     * @param id the id of the auditStatDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/audit-stats/{id}")
    public ResponseEntity<Void> deleteAuditStat(@PathVariable Long id) {
        log.debug("REST request to delete AuditStat : {}", id);
        auditStatService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/audit-stats?query=:query : search for the auditStat corresponding
     * to the query.
     *
     * @param query the query of the auditStat search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/audit-stats")
    public ResponseEntity<List<AuditStatDTO>> searchAuditStats(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of AuditStats for query {}", query);
        Page<AuditStatDTO> page = auditStatService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/audit-stats");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
