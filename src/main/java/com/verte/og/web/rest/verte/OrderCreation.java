package com.verte.og.web.rest.verte;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/Order")
public class OrderCreation {

	@Autowired
	private OrderCreationService orderCreationService;

	@PostMapping("/generate")
	public String generate(@RequestBody OrderGenVM ordGenVM) throws Exception {

		String runid = LocalDateTime.now().toString();
		ArrayList<MyPair[]> myPairList = orderCreationService.generate(runid, ordGenVM);
		sendRestOrder(runid,ordGenVM,myPairList);
		
		return runid; 
	}
	

	public void sendRestOrder(String runid, OrderGenVM ordGenVM, ArrayList<MyPair[]> mypairList) throws Exception {
		
	   for (Iterator iterator = mypairList.iterator(); iterator.hasNext();) {
		
		   MyPair[] myPairs = (MyPair[]) iterator.next();
		   orderCreationService.sendRestOrder(runid, ordGenVM, myPairs);
		
	    }
		
	}
}
