package com.verte.og.repository;

import com.verte.og.domain.AuditStat;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the AuditStat entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AuditStatRepository extends JpaRepository<AuditStat, Long>, JpaSpecificationExecutor<AuditStat> {

}
