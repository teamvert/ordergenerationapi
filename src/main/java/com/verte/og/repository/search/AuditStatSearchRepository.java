package com.verte.og.repository.search;

import com.verte.og.domain.AuditStat;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the AuditStat entity.
 */
public interface AuditStatSearchRepository extends ElasticsearchRepository<AuditStat, Long> {
}
