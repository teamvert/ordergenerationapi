package com.verte.og.service.mapper;

import com.verte.og.domain.*;
import com.verte.og.service.dto.AuditStatDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity AuditStat and its DTO AuditStatDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AuditStatMapper extends EntityMapper<AuditStatDTO, AuditStat> {



    default AuditStat fromId(Long id) {
        if (id == null) {
            return null;
        }
        AuditStat auditStat = new AuditStat();
        auditStat.setId(id);
        return auditStat;
    }
}
