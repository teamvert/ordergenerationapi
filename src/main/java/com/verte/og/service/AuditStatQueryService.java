package com.verte.og.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.verte.og.domain.AuditStat;
import com.verte.og.domain.*; // for static metamodels
import com.verte.og.repository.AuditStatRepository;
import com.verte.og.repository.search.AuditStatSearchRepository;
import com.verte.og.service.dto.AuditStatCriteria;
import com.verte.og.service.dto.AuditStatDTO;
import com.verte.og.service.mapper.AuditStatMapper;

/**
 * Service for executing complex queries for AuditStat entities in the database.
 * The main input is a {@link AuditStatCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AuditStatDTO} or a {@link Page} of {@link AuditStatDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AuditStatQueryService extends QueryService<AuditStat> {

    private final Logger log = LoggerFactory.getLogger(AuditStatQueryService.class);

    private final AuditStatRepository auditStatRepository;

    private final AuditStatMapper auditStatMapper;

    private final AuditStatSearchRepository auditStatSearchRepository;

    public AuditStatQueryService(AuditStatRepository auditStatRepository, AuditStatMapper auditStatMapper, AuditStatSearchRepository auditStatSearchRepository) {
        this.auditStatRepository = auditStatRepository;
        this.auditStatMapper = auditStatMapper;
        this.auditStatSearchRepository = auditStatSearchRepository;
    }

    /**
     * Return a {@link List} of {@link AuditStatDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AuditStatDTO> findByCriteria(AuditStatCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<AuditStat> specification = createSpecification(criteria);
        return auditStatMapper.toDto(auditStatRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link AuditStatDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AuditStatDTO> findByCriteria(AuditStatCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<AuditStat> specification = createSpecification(criteria);
        return auditStatRepository.findAll(specification, page)
            .map(auditStatMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AuditStatCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<AuditStat> specification = createSpecification(criteria);
        return auditStatRepository.count(specification);
    }

    /**
     * Function to convert AuditStatCriteria to a {@link Specification}
     */
    private Specification<AuditStat> createSpecification(AuditStatCriteria criteria) {
        Specification<AuditStat> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), AuditStat_.id));
            }
            if (criteria.getRunId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRunId(), AuditStat_.runId));
            }
            if (criteria.getExecutiontime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getExecutiontime(), AuditStat_.executiontime));
            }
            if (criteria.getError() != null) {
                specification = specification.and(buildStringSpecification(criteria.getError(), AuditStat_.error));
            }
            if (criteria.getInput() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInput(), AuditStat_.input));
            }
            if (criteria.getOutput() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOutput(), AuditStat_.output));
            }
            if (criteria.getStartTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStartTime(), AuditStat_.startTime));
            }
            if (criteria.getEndTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEndTime(), AuditStat_.endTime));
            }
            if (criteria.getRunconfig() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRunconfig(), AuditStat_.runconfig));
            }
        }
        return specification;
    }
}
