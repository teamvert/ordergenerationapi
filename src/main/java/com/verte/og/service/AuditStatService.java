package com.verte.og.service;

import com.verte.og.domain.AuditStat;
import com.verte.og.repository.AuditStatRepository;
import com.verte.og.repository.search.AuditStatSearchRepository;
import com.verte.og.service.dto.AuditStatDTO;
import com.verte.og.service.mapper.AuditStatMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing AuditStat.
 */
@Service
@Transactional
public class AuditStatService {

    private final Logger log = LoggerFactory.getLogger(AuditStatService.class);

    private final AuditStatRepository auditStatRepository;

    private final AuditStatMapper auditStatMapper;

    private final AuditStatSearchRepository auditStatSearchRepository;

    public AuditStatService(AuditStatRepository auditStatRepository, AuditStatMapper auditStatMapper, AuditStatSearchRepository auditStatSearchRepository) {
        this.auditStatRepository = auditStatRepository;
        this.auditStatMapper = auditStatMapper;
        this.auditStatSearchRepository = auditStatSearchRepository;
    }

    /**
     * Save a auditStat.
     *
     * @param auditStatDTO the entity to save
     * @return the persisted entity
     */
    public AuditStatDTO save(AuditStatDTO auditStatDTO) {
        log.debug("Request to save AuditStat : {}", auditStatDTO);
        AuditStat auditStat = auditStatMapper.toEntity(auditStatDTO);
        auditStat = auditStatRepository.save(auditStat);
        AuditStatDTO result = auditStatMapper.toDto(auditStat);
        auditStatSearchRepository.save(auditStat);
        return result;
    }

    /**
     * Get all the auditStats.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AuditStatDTO> findAll(Pageable pageable) {
        log.debug("Request to get all AuditStats");
        return auditStatRepository.findAll(pageable)
            .map(auditStatMapper::toDto);
    }


    /**
     * Get one auditStat by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<AuditStatDTO> findOne(Long id) {
        log.debug("Request to get AuditStat : {}", id);
        return auditStatRepository.findById(id)
            .map(auditStatMapper::toDto);
    }

    /**
     * Delete the auditStat by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete AuditStat : {}", id);
        auditStatRepository.deleteById(id);
        auditStatSearchRepository.deleteById(id);
    }

    /**
     * Search for the auditStat corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AuditStatDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of AuditStats for query {}", query);
        return auditStatSearchRepository.search(queryStringQuery(query), pageable)
            .map(auditStatMapper::toDto);
    }
}
