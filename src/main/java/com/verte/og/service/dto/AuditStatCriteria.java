package com.verte.og.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the AuditStat entity. This class is used in AuditStatResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /audit-stats?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AuditStatCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter runId;

    private LongFilter executiontime;

    private StringFilter error;

    private StringFilter input;

    private StringFilter output;

    private InstantFilter startTime;

    private InstantFilter endTime;

    private StringFilter runconfig;

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getRunId() {
        return runId;
    }

    public void setRunId(StringFilter runId) {
        this.runId = runId;
    }

    public LongFilter getExecutiontime() {
        return executiontime;
    }

    public void setExecutiontime(LongFilter executiontime) {
        this.executiontime = executiontime;
    }

    public StringFilter getError() {
        return error;
    }

    public void setError(StringFilter error) {
        this.error = error;
    }

    public StringFilter getInput() {
        return input;
    }

    public void setInput(StringFilter input) {
        this.input = input;
    }

    public StringFilter getOutput() {
        return output;
    }

    public void setOutput(StringFilter output) {
        this.output = output;
    }

    public InstantFilter getStartTime() {
        return startTime;
    }

    public void setStartTime(InstantFilter startTime) {
        this.startTime = startTime;
    }

    public InstantFilter getEndTime() {
        return endTime;
    }

    public void setEndTime(InstantFilter endTime) {
        this.endTime = endTime;
    }

    public StringFilter getRunconfig() {
        return runconfig;
    }

    public void setRunconfig(StringFilter runconfig) {
        this.runconfig = runconfig;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AuditStatCriteria that = (AuditStatCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(runId, that.runId) &&
            Objects.equals(executiontime, that.executiontime) &&
            Objects.equals(error, that.error) &&
            Objects.equals(input, that.input) &&
            Objects.equals(output, that.output) &&
            Objects.equals(startTime, that.startTime) &&
            Objects.equals(endTime, that.endTime) &&
            Objects.equals(runconfig, that.runconfig);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        runId,
        executiontime,
        error,
        input,
        output,
        startTime,
        endTime,
        runconfig
        );
    }

    @Override
    public String toString() {
        return "AuditStatCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (runId != null ? "runId=" + runId + ", " : "") +
                (executiontime != null ? "executiontime=" + executiontime + ", " : "") +
                (error != null ? "error=" + error + ", " : "") +
                (input != null ? "input=" + input + ", " : "") +
                (output != null ? "output=" + output + ", " : "") +
                (startTime != null ? "startTime=" + startTime + ", " : "") +
                (endTime != null ? "endTime=" + endTime + ", " : "") +
                (runconfig != null ? "runconfig=" + runconfig + ", " : "") +
            "}";
    }

}
