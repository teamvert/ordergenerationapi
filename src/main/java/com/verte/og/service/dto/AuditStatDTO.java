package com.verte.og.service.dto;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the AuditStat entity.
 */
public class AuditStatDTO implements Serializable {

    private Long id;

    @Size(max = 100)
    private String runId;

    private Long executiontime;

    @Size(max = 1000)
    private String error;

    @Size(max = 1000)
    private String input;

    @Size(max = 1000)
    private String output;

    private Instant startTime;

    private Instant endTime;

    @Size(max = 10000)
    private String runconfig;

     public AuditStatDTO() {
    	
    }
    

    public AuditStatDTO(Long id, String runId, Long executiontime, String error, String input, String output,
			Instant startTime, Instant endTime, String runconfig) {
		super();
		this.id = id;
		this.runId = runId;
		this.executiontime = executiontime;
		this.error = error;
		this.input = input;
		this.output = output;
		this.startTime = startTime;
		this.endTime = endTime;
		this.runconfig = runconfig;
	}



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRunId() {
        return runId;
    }

    public void setRunId(String runId) {
        this.runId = runId;
    }

    public Long getExecutiontime() {
        return executiontime;
    }

    public void setExecutiontime(Long executiontime) {
        this.executiontime = executiontime;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public void setStartTime(Instant startTime) {
        this.startTime = startTime;
    }

    public Instant getEndTime() {
        return endTime;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }

    public String getRunconfig() {
        return runconfig;
    }

    public void setRunconfig(String runconfig) {
        this.runconfig = runconfig;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AuditStatDTO auditStatDTO = (AuditStatDTO) o;
        if (auditStatDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), auditStatDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AuditStatDTO{" +
            "id=" + getId() +
            ", runId='" + getRunId() + "'" +
            ", executiontime=" + getExecutiontime() +
            ", error='" + getError() + "'" +
            ", input='" + getInput() + "'" +
            ", output='" + getOutput() + "'" +
            ", startTime='" + getStartTime() + "'" +
            ", endTime='" + getEndTime() + "'" +
            ", runconfig='" + getRunconfig() + "'" +
            "}";
    }
}
